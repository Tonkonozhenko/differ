class Differ
  class LineRecord < Struct.new(:value, :previous)
  end

  class DiffRecord < Struct.new(:value, :type, :old_pos, :new_pos, :details)
  end

  LINE_REMOVED = 'removed'.freeze
  LINE_ADDED = 'added'.freeze
  LINE_SAME = 'same'.freeze
  LINE_MODIFIED = 'modified'.freeze

  attr_reader :old_lines, :new_lines

  def initialize(old_lines, new_lines)
    @old_lines = old_lines
    @new_lines = new_lines

    raise ArgumentError, 'old_lines must be an array' unless old_lines.is_a?(Array)
    raise ArgumentError, 'new_lines must be an array' unless new_lines.is_a?(Array)
  end

  def lcs
    old_length, new_length = old_lines.length, new_lines.length

    m = Array.new(old_length + 1) do
      Array.new(new_length + 1) { LineRecord.new }
    end

    0.upto(old_length) { |i| m[i][0].value = 0 }
    0.upto(new_length) { |j| m[0][j].value = 0 }

    1.upto(old_length) do |i|
      1.upto(new_length) do |j|
        if old_lines[i - 1] == new_lines[j - 1]
          m[i][j].value = m[i - 1][j - 1].value + 1
          m[i][j].previous = [-1, -1]
        elsif m[i - 1][j].value >= m[i][j - 1].value
          m[i][j].value = m[i - 1][j].value
          m[i][j].previous = [-1, 0]
        else
          m[i][j].value = m[i][j - 1].value
          m[i][j].previous = [0, -1]
        end
      end
    end

    i, j, lcs = old_length, new_length, []

    until m[i][j].previous.nil? do
      lcs << old_lines[i - 1] if m[i][j].previous == [-1, -1]
      tmp = i
      i += m[i][j].previous[0]
      j += m[tmp][j].previous[1]
    end

    lcs.reverse
  end

  def diff
    lcs, diff = self.lcs, []

    new_index, old_index, lcs_index = 0, 0, 0

    until old_lines[old_index].nil? && new_lines[new_index].nil? && lcs[lcs_index].nil?
      if old_lines[old_index] != lcs[lcs_index]
        diff << DiffRecord.new(old_lines[old_index], LINE_REMOVED, old_index + 1, new_index + 1)
        old_index += 1
      elsif new_lines[new_index] != lcs[lcs_index]
        # Index of first line in block of removed lines
        i = nil
        (diff.length - 1).downto(0) do |_i|
          if diff[_i].type == LINE_REMOVED
            i = _i
          else
            break
          end
        end

        if i
          diff[i] = DiffRecord.new(diff[i].value, LINE_MODIFIED, diff[i].old_pos, new_index + 1, to: new_lines[new_index])
        else
          diff << DiffRecord.new(new_lines[new_index], LINE_ADDED, old_index + 1, new_index + 1)
        end
        new_index += 1
      else
        diff << DiffRecord.new(lcs[lcs_index], LINE_SAME, old_index + 1, new_index + 1)
        old_index += 1
        new_index += 1
        lcs_index += 1
      end
    end

    diff
  end

  def output
    diff.map do |r|
      case r.type
      when LINE_REMOVED
        "#{r.old_pos} - #{r.value}"
      when LINE_ADDED
        "#{r.old_pos} + #{r.value}"
      when LINE_SAME
        "#{r.old_pos}   #{r.value}"
      when LINE_MODIFIED
        "#{r.old_pos} * #{r.value}|#{r.details[:to]}"
      end
    end.join($/)
  end
end
