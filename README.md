# Differ #

Little command-line diff utility written in Ruby

### Usage ###

```ruby
require 'differ'

filename1 = 'file1.txt'
filename2 = 'file2.txt'
differ = Differ.new(File.read(filename1), File.read(filename2))
puts differ.output
```