require 'spec_helper'

describe Differ do
  let(:strings) { [
    <<-DOC1.lines.map(&:strip), <<-DOC2.lines.map(&:strip)
Some
Simple
Text 
File
    DOC1
Another
Text
File
With
Additional
Lines
    DOC2
  ] }
  let(:differ) { Differ.new(*strings) }

  context '.new' do
    subject { differ }

    it { is_expected.to be_instance_of(Differ) }

    context 'wrong args' do
      let(:strings) { [] }

      it 'raises exception' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end
  end

  context '#lcs' do
    subject { differ.lcs }

    it 'works' do
      is_expected.to eq ['Text', 'File']
    end
  end

  context '#diff' do
    subject { differ.diff }

    it 'works' do
      diff_records = [
        ['Some', 'modified', 1, 1, {'to': 'Another'}],
        ['Simple', 'removed', 2, 1],
        ['Text', 'same', 3, 2],
        ['File', 'same', 4, 3],
        ['With', 'added', 5, 4],
        ['Additional', 'added', 5, 5],
        ['Lines', 'added', 5, 6]
      ].map { |e| Differ::DiffRecord.new(*e) }

      is_expected.to eq(diff_records)
    end
  end

  context '#output' do
    subject { differ.output }

    it 'works' do
      is_expected.to eq(<<-DOC.strip)
1 * Some|Another
2 - Simple
3   Text
4   File
5 + With
5 + Additional
5 + Lines
      DOC
    end
  end
end
